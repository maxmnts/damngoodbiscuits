<!DOCTYPE html>
<html>
    <head>
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="list-user" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message alert alert-info alert-dismissable" role="status">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ${flash.message}
                 </div>
            </g:if>

            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <g:sortableColumn property="username" title="Username" />
                    <g:sortableColumn property="fullName" title="Full Name" />
                    <g:sortableColumn property="enabled" title="Enabled" />
                    <g:sortableColumn property="accountExpired" title="Account Expired" />
                    <g:sortableColumn property="accountLocked" title="Account Locked" />
                    <g:sortableColumn property="passwordExpired" title="Password Expired" />
                </tr>
                <g:each in="${userList}" var="user">
                <tr>
                    <td><g:link action="show" id="${user.id}">${user.username}</g:link></td>
                    <td>${user.fullName}</td>
                    <td>${user.enabled}</td>
                    <td>${user.accountExpired}</td>
                    <td>${user.accountLocked}</td>
                    <td>${user.passwordExpired}</td>
                </tr>
                </g:each>
            </table>
            </div>

            <g:link class="create btn btn-primary" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link>

            <div class="pagination">
                <g:paginate total="${userCount ?: 0}" />
            </div>
        </div>
    </body>
</html>