<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <g:layoutTitle default="Damn Good Biscuits" />
    </title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300|Impact|Noto+Sans' rel='stylesheet' type='text/css'>
    <asset:stylesheet src="application.css" />

    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" />
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" />
		<![endif]-->

    <g:layoutHead/>
</head>

<body>

    <div class="container dgb-background">
        <div id="header">
            <div role="navigation" class="navbar navbar-default visible-xs-block visible-sm-block">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
								</button>
                        <a href="/" class="navbar-brand">Damn Good Biscuits</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="${actionName.equals('index') ? 'active' : ''}">
                                <a href="/">Home</a>
                            </li>
                            <li class="${actionName.equals('list') ? 'active' : ''}">
                                <a href="/blog">Blog</a>
                            </li>
                            <li>
                                <g:link class="" controller="post" action="list" params="[postState: 'archived']">
                                    Archive
                                </g:link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- navbar.navbar-default.visible-xs-block.visible-sm-block -->

            <div class="site-banner visible-md-block visible-lg-block">
                <a href="/">
                    <div class="logo-container"><img src="${assetPath(src: 'logo.png')}" alt="Damn Good Biscuits Logo" /></div>
                </a>
            </div>
        </div>
        <!-- div.header -->

        <div id="body">
            <!--
					Flash messages allow you to display once-off status messages to users, e.g. form
					validation errors, success messages, etc
				-->
            <!-- {{#if messages}}
				<div id="flash-messages" class="container">
					{{#if messages.info}}
					<div class="alert alert-info">
							{{{flashMessages messages.info}}}
					</div>
					{{/if}}
					{{#if messages.success}}
					<div class="alert alert-success">
							{{{flashMessages messages.success}}}
					</div>
					{{/if}}
					{{#if messages.warning}}
					<div class="alert alert-warning">
							{{{flashMessages messages.warning}}}
					</div>
					{{/if}}
					{{#if messages.error}}
					<div class="alert alert-danger">
							{{{flashMessages messages.error}}}
					</div>
					{{/if}}
				</div>
				{{/if}} -->

            <div class="row">

                <div class="container">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="visible-xs-block visible-sm-block" style="margin-bottom:20px;">
                            <button class="btn btn-primary btn-block" style="font-size:1.1em" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
									Categories
								</button>
                            <div class="collapse" id="collapseExample">
                                <div class="list-group">
                                    <g:categoriesList />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-lg-2 visible-md-block visible-lg-block" style="margin-right:40px;">
                    <div>
                        <div class="nav-div">
                            <ul class="list-inline text-center page-menu">
                                <li>
                                    <a href="/" class="text-uppercase  ${actionName.equals('index') ? 'active' : ''}">Home</a>
                                </li>
                                <li>
                                    <g:link controller="post" action="list" class="text-uppercase ${actionName.equals('list') ? 'active' : ''}">Blog</g:link>
                                </li>
                            </ul>
                            <div class="list-group">
                                <g:categoriesList />
                            </div>
                            <ul class="list-inline text-center page-menu" style="padding-top: 18px">
                                <li>
                                    <g:link class="text-uppercase" controller="post" action="list" params="[postState: 'archived']">
                                        Archive
                                    </g:link>
                                </li>
                            </ul>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                <g:archivedBlogPostsByYear />

                            </div>
                        </div>
                        <div class="shape-outer">
                            <div class="shape-inner"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8" style="margin-top:30px;">

                    <g:layoutBody/>

                </div>
            </div>

        </div>
        <!-- div.body -->
        <div id="footer">
            <ul class="list-inline" style="float:left;">
                <li><a href="/about-the-author">About the Author</a></li>
                <li><a href="/privacy-policy">Privacy Policy</a></li>
            </ul>
            <!-- <p style="float:right;">Baked with <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> by Max and Ethan.</p> -->
        </div>
    </div>
    <!-- div.container.dgb-background -->


    <asset:javascript src="application.js" />
    <!-- <script src="/js/custom.js"></script> -->
</body>

</html>