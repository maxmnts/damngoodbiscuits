<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <g:layoutTitle default="DGB Dashboard" />
    </title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <asset:stylesheet src="admin.css" />
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300|Impact|Noto+Sans' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" />
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" />
		<![endif]-->

    <g:layoutHead/>
</head>

<body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                <g:link url="/" class="navbar-brand">Damn Good Biscuits</g:link>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><g:link controller="staticPage" action="adminDashboard">Dashboard</g:link></li>
                    <li>
                        <g:link controller="post">Posts</g:link>
                    </li>
                    <li>
                        <g:link controller="category">Categories</g:link>
                    </li>
                    <sec:access expression="hasRole('ROLE_ADMIN')">
                    <li>
                        <g:link controller="user">Users</g:link>
                    </li>
                    </sec:access>
                    <li>
                        <g:link class="btn btn-danger" url="/logout">Logout</g:link>
                    </li>
                </ul>
                <!--<form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search...">
                </form>-->
            </div>
        </div>
    </nav>


    <div id="body">    
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-2">
                    
                    <g:layoutBody/>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- div.body -->
    <div id="footer">
        <p style="float:right;margin-right:70px;">
            <strong>App Version:</strong> <g:meta name="info.app.version"/> </br>
            <strong>Grails Version:</strong> <g:meta name="info.app.grailsVersion"/>
        </p>
    </div>


    <asset:javascript src="application.js" />
    <!-- <script src="/js/custom.js"></script> -->
</body>

</html>