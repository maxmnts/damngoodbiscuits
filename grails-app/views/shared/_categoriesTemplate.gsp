<g:set var="actv" value="${it.title.equals(params.categoryTitle)}" />
<g:link class="list-group-item ${actv ? 'active' : ''}" controller="post" action="list" params="[categoryTitle: it.title]">
    ${it.title}
</g:link>