<select name="categories" multiple="true" class="select_catg form-control" id="categories">
    <g:each in="${categoryList}" var="catg">
        <option ${this.post?.categories?.contains(catg) ? 'selected' : ''} value="${catg.id}">${catg.title}</option>
    </g:each>
</select>