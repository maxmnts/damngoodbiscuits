<select name="author" class="form-control" id="categories">
    <g:each in="${usersList}" var="user">
        <option ${user.equals(this.post?.author) ? 'selected' : ''} value="${user.id}">${user.fullName}</option>
    </g:each>
</select>