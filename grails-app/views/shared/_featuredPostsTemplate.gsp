<g:if test="${post?.imgUrl}">
    <div class="post-img">
        <img src="${post?.imgUrl}" class="img-responsive center-block img-thumbnail" alt="Post Image" />
        <div class="featured-title-container">
            <h3 style="margin-top:10px;">
                <g:link class="featured-title" mapping="blogPost" params="[id: post?.id, titleFmt: seo.convert(value: post?.title) ]">
                    ${post?.title}
                </g:link>
            </h3>
        </div>
    </div>
</g:if>