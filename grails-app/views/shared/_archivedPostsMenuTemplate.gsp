<div class="panel panel-default">
    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${it}" aria-expanded="false" aria-controls="collapse${it}">
        <div class="panel-heading" role="tab" id="heading${it}">
            <h4 class="panel-title">
                ${it}
            </h4>
        </div>
    </a>
    <div id="collapse${it}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${it}">
        <div class="panel-body" id="archived-panel-body">
            <ul class="list-unstyled" style="padding-left: 9px;">
            
                <g:archivedPostList blogYear="${it}"  />
                
            </ul>
        </div>
    </div>
</div>
