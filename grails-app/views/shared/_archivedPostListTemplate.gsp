<li>
<g:link controller="post" action="list" params="[archivedMonth: g.formatDate(format: 'yyyy-MM', date: it) ]">
    ${g.formatDate(format: 'MMMM', date: it)}
</g:link>
</li>