<!doctype html>
<html>

<head>
    <title>Page Not Found</title>
    <meta name="layout" content="main">
    <g:if env="development">
        <asset:stylesheet src="errors.css" />
    </g:if>
</head>

<body>
    <!--<ul class="errors">
            <li>Error: Page Not Found (404)</li>
            <li>Path: ${request.forwardURI}</li>
        </ul>-->
    <div class="row">
        <h1 style="color:rgb(39, 170, 226);margin-bottom:10px;">Page Not Found! (404)</h1>
        <p style="color:white;">
            <strong>You've come to the wrong place :( <br> But since you're already here, please enjoy this <em style="text-decoration:underline;">Disco</em> version of the <span style="color:#fdb316;">Star Wars Theme</span>!</strong>
        </p>
        <div class="col-md-11 col-md-offset-1">
            <!--<iframe src="http://www.staggeringbeauty.com/" style="border: 1px inset #ddd" width="100%" height="598"></iframe>-->
            <!--<img src="http://www.imabearetc.com/wp-content/uploads/2010/03/willis.jpg">-->
            <!--<iframe width="560" height="315" src="https://www.youtube.com/embed/UkSPUDpe0U8?autoplay=1#t=11s" frameborder="0" allowfullscreen></iframe>-->
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/dWRWYYt47RI?list=PL218D56CD7AC2A0FB" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <p style="color:white;">
        <strong>&hellip; or you can search for the blog post you were looking for using the search bar below:</strong>
    </p>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 ">
            <g:form action="list" class="form-horizontal">
                <fieldset class="form">
                    <div class="input-group">
                        <g:textField name="searchTitle" id="search-field" class="form-control" value="${params.searchTitle?: params.searchTitle}" placeholder="Search by Title ..." />
                        <span class="input-group-btn">
                            <g:submitButton name="search" class="btn btn-primary" value="Search" />
                        </span>
                    </div>
                </fieldset>
            </g:form>
        </div>
    </div>
    <p style="color:white;">
        <strong>(But seriously, you should consider listening.)</strong>
    </p>
</body>

</html>