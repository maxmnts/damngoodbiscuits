<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

    <!-- <div id="content" role="main"> -->

    <div class="row post" style="background-color:white;padding-bottom:30px;">
    	<h2 style="text-align: center; margin-bottom: 1.2em;">About The Author</h2>
    	<div class="col-md-10 col-md-offset-1">
        <p><span style="font-size: 14pt;"><span style="color: #000000;"><span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 20px;"><img class="img-responsive center-block img-thumbnail" src="http://res.cloudinary.com/dm3tnklis/image/upload/v1454575459/k6i2edpae2ya8r5iovmq.jpg" alt=""></span></span></span></p>
        <p>&nbsp;</p>
        <p><span style="color: #000000; font-size: 14pt;"><span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 20px;">HI EVERYBODY! My name is Ben Smith, and I'm a self-professed&nbsp;</span>and widely-recognized-by-my-colleagues pop culture nerd and critic.<span style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 20px;">&nbsp;</span>A "walking Marvel Comic and movie encyclopedia," if I may paraphrase a quote by my old college roommates. I grew up a giant film&nbsp;and comic fan and my passion has only intensified in the years since. I like to think all this has helped me form my own beneficial opinions to determine what is or isn't&nbsp;cinema gold or at least enjoyable entertainment. If you wish to contact me my email is benjamminsmith30@gmail.com or message me on the Damn Good Biscuits&nbsp;<a href="https://www.facebook.com/damngoodbiscuits/">Facebook</a> and&nbsp;<a href="https://twitter.com/DGoodBiscuits">Twitter</a> page.&nbsp;</span></p>
      </div>
    </div>
    <div class="space" style="height: 50vh;"></div>

    <!-- </div> -->

</body>
</html>
