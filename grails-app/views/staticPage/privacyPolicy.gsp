<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

    <!-- <div id="content" role="main"> -->

    <div class="row post" style="background-color:white;padding-bottom:30px;">
    	<h2 style="text-align: center; margin-bottom: 1.2em;">Privacy Policy</h2>
    	<div class="col-md-10 col-md-offset-1">
        <p><span style="font-size: 12pt;">This blog does not share personal information with third parties nor do we store any information about your visit to this blog other than to analyze and optimize your content and reading experience through the use of cookies.</span></p>
        <p><span style="font-size: 12pt;">You can turn off the use of cookies at anytime by changing your specific browser settings.</span></p>
        <p><span style="font-size: 12pt;">We are not responsible for republished content from this blog on other blogs or websites without our permission.</span></p>
        <p><span style="font-size: 12pt;">This privacy policy is subject to change without notice and was last updated on January&nbsp;30, 2016. If you have any questions, feel free to contact me directly here: benjamminsmith30@gmail.com.</span></p>
    	</div>
    </div>
    <div class="space" style="height: 50vh;"></div>

    <!-- </div> -->

</body>
</html>
