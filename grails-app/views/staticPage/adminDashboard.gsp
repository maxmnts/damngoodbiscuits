<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="admin" />
    <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}" />
    <title>
        <g:message code="default.list.label" args="[entityName]" />
    </title>
</head>

<body>
    <h1>Dashboard</h1>
    <hr />
    <h2><g:link controller="post">Posts</g:link></h2>
    <ul class="">
        <li>
            <g:link controller="post" action="create">Create a Blog Post</g:link>
        </li>
    </ul>
    <h2><g:link controller="category">Categories</g:link></h2>
    <ul class="">
        <li>
            <g:link controller="category" action="create">Create a Category</g:link>
        </li>
    </ul>

</body>

</html>