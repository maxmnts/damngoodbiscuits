<!doctype html>
<html>

<head>
    <meta name="layout" content="main" />
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>

<body>

    <!-- <div id="content" role="main"> -->
    <div class="row slider-container">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1">
            <div id="feature-header">
                <p class="text-lowercase">Featured Posts</p>
            </div>
            <div class="dgb-slider">
                <g:featuredBlogPosts/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="about-outer">
            <div class="about-inner">
                <p>Mission Statement</p>
            </div>
        </div>
    </div>

    <div class="row post about-author">
        <div class="col-md-3">
            <img src="${assetPath(src: 'jeremiah_johnson_biscuits.jpg')}" alt="Ben! (The Author)" class="img-responsive center-block img-rounded" style="margin-bottom:10px;" />
        </div>
        <div class="col-md-9">
            <p>Welcome to <strong>Damn Good Biscuits</strong>! (It's a movie quote, not a declaration of my personal cooking skills.) This is one über-nerd's opinion/review fan-site about movies, TV shows, or comic books: the ones that are worth seeing,
                the ones worth drooling about, and the ones you should steer clear of because they will probably personally disrespect you. Hopefully you find my critical insight and rants helpful when you venture out into the comfortable seats of your
                local cinema (or the couch in front of your television, whichever is more your speed)!</p>
            <p><strong>Like us on <a href="https://www.facebook.com/damngoodbiscuits/">Facebook</a>
    				or follow us on <a href="https://twitter.com/DGoodBiscuits">Twitter</a>!</strong></p>
        </div>
    </div>

    <div class="space" style="height: 50vh;"></div>
    <!-- </div> -->
</body>

</html>