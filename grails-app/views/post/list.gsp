<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <!-- <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title> -->
    </head>
    <body>
        <div id="list-post" class="content scaffold-list blog" role="main">
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            <div class="row">
                <div class="col-lg-7">
                    <div class="pagination">
                        <g:paginate total="${postCount ?: 0}" max="7" params="[categoryTitle: params.categoryTitle]" />
                    </div>
                </div>
                <div class="col-lg-5">
                    <g:form action="list" class="form-horizontal">
                        <fieldset class="form">
                            <div class="input-group">
                                <g:textField name="searchTitle" id="search-field" class="form-control" value="${params.searchTitle?: params.searchTitle}" placeholder="Search by Title ..." />
                                <span class="input-group-btn">
                                    <g:submitButton name="search" class="btn btn-primary" value="Search" />
                                </span>
                            </div>
                        </fieldset>
                    </g:form>
                </div>
            </div>

            <g:render template="postsTemplate" var="post" collection="${posts}" />

            <div class="pagination pull-right" style="margin-top:23px;" >
                <g:paginate total="${postCount ?: 0}" max="7" params="[categoryTitle: params.categoryTitle]" />
            </div>
        </div>
        <div class="space" style="height: 50vh;"></div>
    </body>
</html>
