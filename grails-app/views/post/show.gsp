<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="main" />
    <meta property="fb:app_id" content="1610832099239801" />
    <!-- <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}" />-->
    <title>${post?.title ? 'Damn Good Biscuits - ' + post?.title : 'Blog Post'}</title>
</head>

<body>
    <!-- Facebook Share and Comments-->
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '1610832099239801',
                xfbml: true,
                version: 'v2.5'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <article>
        <div class="row post">
            <div class="col-md-10 col-md-offset-1">

                <g:render template="modifyPostTemplate" />


                <g:if test="${post?.imgUrl}">
                    <div class="post-img">
                        <img src="${post.imgUrl}" class="img-responsive center-block img-thumbnail" alt="${post?.title}'s banner image.'" />
                    </div>
                </g:if>


                <h1>${post?.title}</h1>
                <p class="text-muted" id="post-meta-details">
                    by ${post?.author.fullName} published on
                    <g:formatDate format="MMM d, yyyy" date="${post?.publishedDate}" />
                </p>


                ${raw(post?.content)}


                <hr>
                <ul class="list-inline">
                    <li>
                        <div class="fb-share-button" data-href="http://www.damngoodbiscuits.com${request.forwardURI}" data-layout="button"></div>
                    </li>
                    <li><a href="https://twitter.com/share" class="twitter-share-button" data-via="DGoodBiscuits" data-count="none" data-hashtags="damngoodbiscuits">Tweet</a></li>
                </ul>
                <script>
                    ! function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0],
                            p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + '://platform.twitter.com/widgets.js';
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, 'script', 'twitter-wjs');
                </script>
            </div>
        </div>
    </article>

    <article>
        <div class="row">
            <div class="post" style="padding:25px;">
                <div class="fb-comments" data-href="http://www.damngoodbiscuits.com${request.forwardURI}" data-numposts="7" data-width="100%"></div>
            </div>
        </div>
    </article>

    <div class="space" style="height: 50vh;"></div>

</body>
</html