<sec:ifLoggedIn>
    <fieldset class="buttons pull-right">
        <g:link class="edit btn btn-primary" style="color:white;" action="edit" resource="${this.post}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
    </fieldset>
</sec:ifLoggedIn>
