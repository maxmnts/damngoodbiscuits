<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="admin" />
        <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="list-post" class="content scaffold-list blog" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                ${flash.message}
                </div>
            </g:if>
            
            <div style="margin:15px 0;">
                <g:link class="btn btn-primary" controller="post" action="create">Create Post</g:link>
                <div class="pagination pull-right">
                    <g:paginate total="${postCount ?: 0}" />
                </div>
            </div>

            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <g:sortableColumn property="title" title="Title" />
                    <g:sortableColumn property="postState" title="Post State" />
                    <th>Author</h>
                    <g:sortableColumn property="publishedDate" title="Published Date" />
                    <th></th>
                </tr>
                <g:each in="${postList}" var="post">
                <tr>
                    <td>${post.id}</td>
                    <td><g:link action="show" id="${post.id}">${post.title}</g:link></td>
                    <td>${post.postState}</td>
                    <td>${post.author.fullName}</td>
                    <td>${post.publishedDate.format('MMM dd, yyyy')}</td>
                    <td>
                    <g:form resource="${post}" method="DELETE">
                        <fieldset class="buttons">
                            <g:link class="edit btn btn-primary btn-sm" style="color:white;" action="edit" resource="${post}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                            <input class="delete btn btn-danger btn-sm" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </fieldset>
                    </g:form>
                    </td>
                </tr>
                </g:each>
            </table>
            </div>

            <div>
                <g:link class="btn btn-primary" controller="post" action="create">Create Post</g:link>
                <div class="pagination pull-right"  >
                    <g:paginate total="${postCount ?: 0}" />
                </div>
            </div>
        </div>
    </body>
</html>
