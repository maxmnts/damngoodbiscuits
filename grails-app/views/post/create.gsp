<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="admin" />
    <g:set var="entityName" value="${message(code: 'post.label', default: 'Post')}" />
    <title>
        <g:message code="default.create.label" args="[entityName]" />
    </title>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script>
        tinymce.init({
            selector: '#blog-brief',
            toolbar: 'fontsizeselect forecolor backcolor | bold italic underline | bullist numlist | outdent indent | link image | code',
            menubar: false,
            plugins: 'code spellchecker textcolor textcolor',
            browser_spellcheck: true
        });
    </script>
    <script>
        tinymce.init({
            selector: '#blog-content',
            toolbar: 'fontsizeselect forecolor backcolor | bold italic underline | bullist numlist | outdent indent | link image | code | searchreplace',
            menubar: 'format',
            plugins: 'code spellchecker textcolor textcolor link searchreplace wordcount image imagetools',
            browser_spellcheck: true,
            relative_urls: false,
            image_class_list: [
                {title: 'None', value: ''},
                {title: 'Center Image', value: 'img-responsive center-block img-thumbnail'}
            ],
            setup: function(editor) {
                initImageUpload(editor);
            },
            file_browser_callback: function(field_name, url, type, win) {
                var bucket = new AWS.S3({
                    params: {
                        Bucket: 'dgoodbiscuits-images'
                    }
                });
                var fileChooser = document.getElementById('tinymce-uploader');
                var srcTextBox = win.document.getElementById(field_name);

                fileChooser.click();

                fileChooser.addEventListener('change', function() {
                    var file = fileChooser.files[0];

                    if (file) {
                        var params = {
                            Key: file.name,
                            ContentType: file.type,
                            Body: file,
                            ACL: "private"
                        };
                        bucket.upload(params, function(err, data) {
                            if (err) {
                                srcTextBox.value  = 'Error uploading image.';
                                srcTextBox.style.border = "1px #a94442 solid";
                            } else {
                                var imgLocation = data.Location;
                                srcTextBox.value = imgLocation;
                                srcTextBox.style.border = "1px #3c763d solid";
                            }
                        });
                    } else {
                        srcTextBox.value = 'Nothing to upload.';
                        srcTextBox.style.border = "1px #bf7b06 solid";
                    }
                }, false);
            }
        }); // Other potential options are: alignleft, aligncenter, alignright, alignjustify, blockquote
        
        function initImageUpload(editor) {
            var inp = $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
            $(editor.getElement()).parent().append(inp);
        }
    </script>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.4.2.min.js"></script>
    <script type="text/javascript">
        // See the Configuring section to configure credentials in the SDK
        AWS.config.update({
            accessKeyId: '${awsAccessKeyId}',
            secretAccessKey: '${awsSecretKey}'
        });

        // Configure your region
        AWS.config.region = 'us-east-1';
    </script>
</head>

<body>
    <div id="create-post" class="content scaffold-create" role="main">
        <h1><g:message code="default.create.label" args="[entityName]" /></h1>
        <g:if test="${flash.message}">
            <div class="message alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> ${flash.message}
            </div>
        </g:if>
        <g:hasErrors bean="${this.post}">
            <div class="alert alert-danger">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.post}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
                            <g:message error="${error}" />
                        </li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="file">Select Image:</label>
                        <input type="file" id="file-chooser" class="form-control" accept="image/*" required/>
                    </div>
                    <button id="upload-button" class="btn">Upload</button>
                    <img src="${this.post?.imgUrl?:resource(dir: 'images', file: 'no_img.jpg')}" id="selectedImg" class="img-responsive center-block img-thumbnail" />
                </div>
                <div id="results"></div>
            </div>
        </div>

        <script type="text/javascript">
            var bucket = new AWS.S3({
                params: {
                    Bucket: 'dgoodbiscuits-images'
                }
            });

            var fileChooser = document.getElementById('file-chooser');
            var button = document.getElementById('upload-button');
            var results = document.getElementById('results');
            button.addEventListener('click', function() {
                var file = fileChooser.files[0];
                if (file) {
                    results.innerHTML = '';

                    var params = {
                        Key: file.name,
                        ContentType: file.type,
                        Body: file,
                        ACL: "private"
                    };
                    bucket.upload(params, function(err, data) {
                        if (err) {
                            results.innerHTML = 'Error uploading image.';
                            button.className += " btn-danger";
                        } else {
                            results.innerHTML = 'Upload successful.';
                            var imgLocation = data.Location;
                            document.getElementById('bannerImage').value = imgLocation;
                            button.className += " btn-success";
                            document.getElementById('selectedImg').setAttribute("src", imgLocation);
                        }
                    });
                } else {
                    results.innerHTML = 'Nothing to upload.';
                }
            }, false);
        </script>


        <g:form action="save" class="form-horizontal">
            <fieldset class="form">

                <div class="form-group">
                    <label for="title" class="col-sm-1 control-label">Title:
                        </label>
                    <div class="col-sm-10">
                        <g:textField name="title" class="form-control" autofocus="true" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="postState" class="col-sm-1 control-label">Post State:
                        </label>
                    <div class="col-sm-10">
                        <g:select name="postState" from="${['draft','published','archived']}" class="form-control" value="${this.post?.postState}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="imgUrl" class="col-sm-1 control-label">Image:
                        </label>
                    <div class="col-sm-10">
                        <g:textField id="bannerImage" name="imgUrl" class="form-control" value="${this.post?.imgUrl}" readonly="true" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="publishedDate" class="col-sm-5 col-sm-offset-1 control-label">Published Date:
                            <g:datePicker name="publishedDate" value="${this.post?.publishedDate ?: new Date()}" precision="minute" 
                                relativeYears="[-1..30]" class="form-control"/>  
                        </label>

                    <label for="featured" class="col-sm-2 control-label">
                        <button id="featuredButton" type="button" class="btn btn-default">Featured?</button>
                        <g:checkBox id="featuredCheck" class="hidden" name="featured" value="${this.post?.featured}"/>
                    </label>
                </div>
                <div class="form-group">
                    <label for="author" class="col-sm-1 control-label">Author:
                        </label>
                    <div class="col-sm-10">
                        <g:selectUserField />
                    </div>
                </div>
                <div class="form-group">
                    <label for="contentBrief" class="col-sm-1 control-label">Content Brief:
                        </label>
                    <div class="col-sm-10">
                        <g:textArea name="contentBrief" rows="4" id="blog-brief" class="form-control" value="${this.post?.contentBrief}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="content" class="col-sm-1 control-label">Content:
                        </label>
                    <div class="col-sm-10">
                        <g:textArea name="content" rows="30" id="blog-content" class="form-control" value="${this.post?.content}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="author" class="col-sm-1 control-label">Categories:
                        </label>
                    <div class="col-sm-10">
                        <g:selectCategoriesField />
                    </div>
                </div>

            </fieldset>
            <fieldset class="buttons">
                <g:submitButton name="create" class="save btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                <g:link controller="post" onclick="return confirm('Are you sure???')" class="btn btn-default">Cancel</g:link>
            </fieldset>
        </g:form>
    </div>
</body>

</html>