<div class="post">
    <g:render template="modifyPostTemplate" />
    <h3 class="title">
        <g:link mapping="blogPost" params="[id: post?.id, titleFmt: seo.convert(value: post?.title) ]">
            ${post?.title}
        </g:link>
    </h3>
    <p class="text-muted" style="font-family:'Oswald', sans-serif; font-weight: 300;">
        Posted in
        <g:each status="i" in="${post?.categories.title}" var="catg">
            <g:link controller="post" action="list" params="[categoryTitle: catg]">
                ${catg}
            </g:link>
            <g:if test='${ i < post?.categories.size()-1 }'>,</g:if>
        </g:each>
        by ${post?.author.fullName}
    </p>
    ${raw(post?.contentBrief)}
    <p class="read-more">
        <g:link mapping="blogPost" params="[id: post?.id, titleFmt: seo.convert(value: post?.title) ]">
            Read more &hellip;
        </g:link>
    </p>
</div>