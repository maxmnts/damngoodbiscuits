<!DOCTYPE html>
<html>
    <head>
        <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="list-category" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message alert alert-info alert-dismissable" role="status">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    ${flash.message}
                </div>
            </g:if>
            
            <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <g:sortableColumn property="title" title="Title" />
                    <th></th>
                </tr>
                <g:each in="${categoryList}" var="catg">
                <tr>
                    <td>${catg.id}</td>
                    <td><g:link action="show" id="${catg.id}">${catg.title}</g:link></td>
                    <td>
                    <g:form resource="${catg}" method="DELETE">
                        <fieldset class="buttons">
                            <g:link class="edit btn btn-primary btn-sm" style="color:white;" action="edit" resource="${catg}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                            <input class="delete btn btn-danger btn-sm" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </fieldset>
                    </g:form>
                    </td>
                </tr>
                </g:each>
            </table>
            </div>
            
            <g:link class="btn btn-primary" action="create">Create Category</g:link>

            <div class="pagination">
                <g:paginate total="${categoryCount ?: 0}" />
            </div>
        </div>
    </body>
</html>