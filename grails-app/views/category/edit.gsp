<!DOCTYPE html>
<html>
    <head>
        <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div id="edit-category" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message alert alert-info alert-dismissable" role="status">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                ${flash.message}
            </div>
            </g:if>
            <g:hasErrors bean="${this.category}">
            <div class="alert alert-danger">
                <ul class="errors" role="alert">
                    <g:eachError bean="${this.category}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
            </g:hasErrors>
            <g:form resource="${this.category}" method="PUT" class="form-horizontal">
                <g:hiddenField name="version" value="${this.category?.version}" />
                <fieldset class="form">
                    <div class="form-group">
                        <label for="title" class="col-sm-1 control-label">Title:
                        </label>
                        <div class="col-sm-10">
                            <g:textField name="title" class="form-control" value="${this.category.title}" />                            
                        </div>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save btn btn-primary" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                    <g:link controller="category" onclick="return confirm('Are you sure???')" class="btn btn-default">Cancel</g:link>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
