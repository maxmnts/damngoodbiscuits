package com.damngoodbiscuits

import java.util.Calendar
import java.text.SimpleDateFormat

import grails.transaction.Transactional

@Transactional
class ListService {

    @Transactional(readOnly = true)
    def listUsers() {
        User.list()
    }
    
    @Transactional(readOnly = true)
    def listCategories() {
        Category.listOrderByTitle(order: "asc")
    }

    @Transactional(readOnly = true)
    def listFeaturedPosts() {
        def results = Post.withCriteria {
            eq("featured", true)
            order("publishedDate", "desc")
        }
        return results
    }

    @Transactional(readOnly = true)
    def getArchivedPostsYear() {
        def results = Post.withCriteria {
            eq("postState", "archived")
            order("publishedDate", "asc")
        }
        
        def years = new ArrayList()
        Calendar cal = Calendar.getInstance()

        for (blogPost in results) {
            cal.setTime(blogPost.publishedDate)
            int yr = cal.get(Calendar.YEAR)
            
            if (!years.contains(yr)){
                years << yr
            }
        }


        return years
    }

    @Transactional(readOnly = true)
    def getArchivedPostsMonths(year) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String startOfYear = year + "-01-01"
        Date minDate = formatter.parse(startOfYear)

        Calendar cal = Calendar.getInstance();
        cal.setTime(minDate);
        cal.set(Calendar.MONTH, 11); // 11 = december
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date maxDate = cal.getTime()

        def results = Post.withCriteria {
            ge("publishedDate", minDate)
            le("publishedDate", maxDate)
            eq("postState", "archived")
            order("publishedDate", "asc")
        }

        def months = new ArrayList()

        for (blogPost in results) {
            cal.setTime(blogPost.publishedDate)
            int month = cal.get(Calendar.MONTH)
            
            // Include one blog post's publishedDate per Month.
            // Only used for generating Month links in Archived Nav Menu.
            if (!months.contains(month)){
                months << blogPost.publishedDate
            }
        }

        return months
    }
}
