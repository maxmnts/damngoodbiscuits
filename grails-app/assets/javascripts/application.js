// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });

        $('.dgb-slider').slick({
            autoplay: true,
            autoplaySpeed: 6000
        });

        $(".spoiler-btn").on('click', function() {
            $(this).next().fadeIn("slow");
            $(this).remove();
        });

        $(".select_catg").select2({
            theme: "bootstrap",
            placeholder: "Select Categories ..."
        });

        // Initalize state
        if ($("#featuredCheck").is(':checked'))
            $("#featuredButton").html("Featured").attr("class", "btn btn-success");
        else
            $("#featuredButton").html("Not Featured").attr("class", "btn btn-default");
        // Set state
        $("#featuredButton").on('click', function() {
            if ($("#featuredCheck").is(':checked')) {
                $("#featuredCheck").prop("checked", false);
                $("#featuredButton").html("Not Featured").attr("class", "btn btn-default");
            } else {
                $("#featuredCheck").prop("checked", true);
                $("#featuredButton").html("Featured").attr("class", "btn btn-success");
            }
        });

    })(jQuery);
}