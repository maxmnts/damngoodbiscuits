package com.damngoodbiscuits

class Post {
  static belongsTo = [author: User]
  static hasMany = [categories: Category]

  boolean featured
  String title
  String imgUrl
  String contentBrief
  String postState
  Date publishedDate
  String content

  static mapping = {
    postState defaultValue: "'draft'"
    content type: "text"
    contentBrief type: "text"
  }

  static constraints = {
    title blank:false
    postState inList: ["draft", "published", "archived"]
    imgUrl url: true
    imgUrl nullable:true
    contentBrief nullable:true
    content nullable:true
  }
}
