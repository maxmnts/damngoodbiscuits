package com.damngoodbiscuits

class CustomTagLib {
    static defaultEncodeAs = [taglib:'raw']
    
    def listService
    
    def categoriesList = { attrs, body ->
        out << render(template: "/shared/categoriesTemplate", collection: listService.listCategories())
    }

    def featuredBlogPosts = { attrs, body ->
        out << render(template: "/shared/featuredPostsTemplate", collection: listService.listFeaturedPosts())
    }

    def archivedBlogPostsByYear = { attrs, body ->
        out << render(template: "/shared/archivedPostsMenuTemplate", collection: listService.getArchivedPostsYear())
    }

    def archivedPostList = { attrs, body ->
        out << render(template: "/shared/archivedPostListTemplate", collection: listService.getArchivedPostsMonths(attrs.blogYear))
    }
    
    def selectUserField = { attrs, body ->
        out << render(template: "/shared/selectUserTemplate", model: [usersList: listService.listUsers()])
    }
    
    def selectCategoriesField = { attrs, body ->
        out << render(template: "/shared/selectCategoriesTemplate", model: [categoryList: listService.listCategories()])
    }
    
}