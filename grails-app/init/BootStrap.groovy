import com.damngoodbiscuits.Role
import com.damngoodbiscuits.User
import com.damngoodbiscuits.UserRole

class BootStrap {

    def init = { servletContext ->
      def adminRole = new Role('ROLE_ADMIN').save()
      def userRole = new Role('ROLE_USER').save()

      def adminUser = new User('Admin User','me', 'password').save()
      def scrubUser = new User('Regular User','foo', 'foobar').save()

      UserRole.create adminUser, adminRole
      UserRole.create scrubUser, userRole

      UserRole.withSession{
        it.flush()
        it.clear()
      }

      // assert User.count() == 2
      // assert Role.count() == 2
      // assert UserRole.count() == 2
    }
    def destroy = {
    }
}
