package damngoodbiscuits

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "500"(view:'/error')
        "404"(view:'/notFound')

        "/"(controller: "StaticPage", view: "index")
        "/blog"(controller: "post", action: "list")
        "/blog/post/$id"(controller: "post", action: "show")
        name blogPost: "/blog/post/$id/$titleFmt?" {
            controller = "post"
            action = "show"
        }

        "/admin"(controller: "StaticPage", action: "adminDashboard")
        "/home"(controller: "StaticPage", action: "index")
        "/about-the-author"(controller: "StaticPage", action: "aboutTheAuthor")
        "/privacy-policy"(controller: "StaticPage", action: "privacyPolicy")
    }
}
