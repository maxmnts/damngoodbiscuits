package com.damngoodbiscuits

import grails.plugin.springsecurity.annotation.Secured

class StaticPageController {

    def index() { }

    def aboutTheAuthor() { }

    def privacyPolicy() { }

    @Secured(['ROLE_ADMIN', 'ROLE_USER'])
    def adminDashboard() { }
}
