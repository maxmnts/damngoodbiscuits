package com.damngoodbiscuits

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class CategoryController {
    static layout = 'admin'
    static scaffold = Category

}
