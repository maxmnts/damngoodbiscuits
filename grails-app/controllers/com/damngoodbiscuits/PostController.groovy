package com.damngoodbiscuits

import java.util.Calendar
import java.text.SimpleDateFormat

import grails.plugin.springsecurity.annotation.Secured

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class PostController {

    static scaffold = Post

    private static AmazonS3 s3
    private static BasicAWSCredentials awsCreds
    private static String accessKey
    private static String secretKey

    public void initializeS3() {
      if (!s3) {
          accessKey = grailsApplication.config.getProperty('aws.accessKey')
          secretKey = grailsApplication.config.getProperty('aws.secretKey')
          awsCreds = new BasicAWSCredentials(accessKey, secretKey)
          s3 = new AmazonS3Client(awsCreds);
      }
    }


    def create() {
        this.initializeS3()

        [params: params, awsAccessKeyId:accessKey, awsSecretKey:secretKey]
    }

    def edit(Post post) {
        this.initializeS3()

        [post: post, awsAccessKeyId:accessKey, awsSecretKey:secretKey]
    }

    def list() {
        def c = Post.createCriteria()
        def results
        def catg = params.categoryTitle?.trim()
        def arvd = params.archivedMonth?.trim()
        def srch = params.searchTitle?.trim()

        if ( catg ){
          results = c.list(max: 7, offset: params.offset) {
            eq("postState", "published")
            categories {
              eq('title', catg)
            }
            order("publishedDate", "desc")
          }
        }
        else if ( srch ){
          results = c.list(max: 7, offset: params.offset) {
            ne("postState", "draft")
            ilike("title", "%${srch}%")
            order("publishedDate", "desc")
          }
        }
        else if ( arvd ){
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
          String startOfMonth = arvd + "-01"
          Date minDate = formatter.parse(startOfMonth)
          
          Calendar cal = Calendar.getInstance();
          cal.setTime(minDate);
          cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
          Date maxDate = cal.getTime()

          results = c.list(max: 7, offset: params.offset) {
            ge("publishedDate", minDate)
            le("publishedDate", maxDate)
              eq("postState", "archived")
            order("publishedDate", "desc")
          }
        }
        else {
          results = c.list(max: 7, offset: params.offset) {
              eq("postState", params.postState?.equals("archived")? "archived" : "published")
            order("publishedDate", "desc")
          }
        }

        [posts: results, postCount: results.getTotalCount()]
    }
}