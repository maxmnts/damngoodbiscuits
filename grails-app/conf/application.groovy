def accessKey = System.getProperty("ACCESS_KEY") ?: System.getenv("ACCESS_KEY")
def secretKey = System.getProperty("SECRET_KEY") ?: System.getenv("SECRET_KEY")
aws.accessKey = accessKey
aws.secretKey = secretKey

def host = System.getenv("DB_HOSTNAME")
def port = System.getenv("DB_PORT")
def dbnm = System.getenv("DB_NAME")
def usernm = System.getenv("DB_USERNAME") ?: System.getenv("USERNAME_MYSQL")
def passwd = System.getenv("DB_PASSWORD") ?: System.getenv("PASSWORD_MYSQL")

// Note: AWS RDS injects via command line parameters, hence when getting a 
// property use System.getProperty("RDS_HOSTNAME")

def url_connection = "jdbc:mysql://${host}:${port}/${dbnm}"

environments {
	development {
		dataSource {
			dbCreate = "update"
			pooled = true
			url = "jdbc:mysql://localhost:3306/damngooddb"
			driverClassName = "com.mysql.jdbc.Driver"
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			username = usernm
			password = passwd
			properties {
				jmxEnabled = true
				initialSize = 5
				maxActive = 50
				minIdle = 5
				maxIdle = 25
				maxWait = 10000
				maxAge = 600000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				validationInterval = 15000
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				jdbcInterceptors = "ConnectionState;StatementCache(max=200)"
			}
		}
	}
	production {
		dataSource {
			username = usernm
			password = passwd
			url = url_connection
			driverClassName = "com.mysql.jdbc.Driver"
			dbCreate = "update"
			pooled = true
			dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			properties {
				jmxEnabled = true
				initialSize = 5
				maxActive = 50
				minIdle = 5
				maxIdle = 25
				maxWait = 10000
				maxAge = 600000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				validationInterval = 15000
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				jdbcInterceptors = "ConnectionState;StatementCache(max=200)"
			}
		}
	}
}

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.damngoodbiscuits.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.damngoodbiscuits.UserRole'
grails.plugin.springsecurity.authority.className = 'com.damngoodbiscuits.Role'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],
	[pattern: '/post/show', 		 access: ['permitAll']],
	[pattern: '/post/list', 		 access: ['permitAll']],
	[pattern: '/staticPage/index', 		 access: ['permitAll']],
	[pattern: '/staticPage/aboutTheAuthor', 		 access: ['permitAll']],
	[pattern: '/staticPage/privacyPolicy', 		 access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]
